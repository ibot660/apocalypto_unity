﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet_Behave : MonoBehaviour
{
    // How far (per second) bullets travel in world coordinates - we can set in Unity as well!
   

    // Reference to the player who fired the bullet
    private Sprite_Fire owner;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Wall")
        {
            Destroy(gameObject);
        }
    }

// Property to store and access the player who fired the bullet
    public Sprite_Fire OwningPlayer
    {
        get { return owner; }
        set { owner = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

       

        // If the bullet reaches the top of the play area remove it from the scene
        if (transform.position.y > 5.0f)
        {
            Destroy(gameObject);
        }
    }
}
