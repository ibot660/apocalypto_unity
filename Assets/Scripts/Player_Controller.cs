﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour
{
    [SerializeField] public float speed = 4.0f;
    public float Increase = 2.0f;
    public float Decrease = -2.0f;

    [SerializeField] private Text livesText = null;
    [SerializeField] private int numLives = 10;


    [SerializeField] private Text ScoreText = null;
    private int score = 0;

    private Rigidbody2D rb;

    private int maxSpeed = 15;

    // Start is called before the first frame update
    void Start()
    {
        ScoreText.text = score.ToString();
        livesText.text = numLives.ToString();
        rb = GetComponent<Rigidbody2D>();
    }

    void movePlayer()
    {

        float leftRightInput = Input.GetAxis("Horizontal");

        float upDownInput = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(leftRightInput, upDownInput);

        rb.AddForce(movement * speed);
        //Vector2 horizontalMoveVector = Vector2.right * leftRightInput;

        //Vector2 verticalMoveVector = Vector2.up * upDownInput;

        //Vector2 moveVector = (horizontalMoveVector + verticalMoveVector) * speed * Time.deltaTime;

        //transform.Translate(moveVector);

    }
    
    // Update is called once per frame
    void Update()
    {
        movePlayer();

        if (rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Different objects might collide with the player - by assigning tags we can work out which object collided...
        // Here we're only interested in enemy lasers - everything else is okay :)
        if (collision.tag == "zombie")
        {
            // If we got hit, decrease the number of lives...
            numLives--;
            // ...and update the game HUD to reflect this
            livesText.text = numLives.ToString();

        }

        if (collision.tag == "Wall")
        {
            Debug.Log("Hello");
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }

        if (collision.tag =="Health")
        {
            numLives++;
            numLives++;
            livesText.text = numLives.ToString();
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Speed")
        {
            speed += Increase;
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Slow")
        {
            speed += Decrease;
            Destroy(collision.gameObject);
        }

    }

    public int numberOfLives()
    {
        return numLives;
    }

    public void updateScore(int points)
    {
        // Increase the score variable
        score += points;

        // Update the game HUD to reflect the new value
        ScoreText.text = score.ToString();
    }
}
