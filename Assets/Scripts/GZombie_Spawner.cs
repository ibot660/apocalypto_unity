﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GZombie_Spawner : MonoBehaviour
{
    [SerializeField] private GameObject Gzombie = null;
    [SerializeField] private float gzombieSpawnTime = 1.0f;

    [SerializeField] private GameController control = null;
    [SerializeField] public GZombie_Spawner factory1 = null;
    [SerializeField] private int numLives = 1;
    private float countdown = 0.0f;
    [SerializeField] private int pointsValue = 10;

    System.Random rndGen;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    void generateGZombie()
    {
        Vector2 Spoint = transform.position;
        GameObject genericzombie = Instantiate(Gzombie, Spoint, Quaternion.identity);

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Different objects might collide with the player - by assigning tags we can work out which object collided...
        // Here we're only interested in enemy lasers - everything else is okay :)
        if (collision.tag == "Player_bullet")
        {
            // If we got hit, decrease the number of lives...
            numLives--;
            // ...and update the game HUD to reflect this

            Destroy(collision.gameObject);
        }

        

    }


    // Update is called once per frame
    void Update()
    {

        countdown -= Time.deltaTime;

        rndGen = new System.Random();


        if (countdown <= 0.0f)
        {
            generateGZombie();
            countdown = gzombieSpawnTime;
        }


    }


    public int numberOfLives()
    {
        return numLives;
    }

    public void destroyfactory1()
    {
        Destroy(gameObject);
    }

    public int getPointsValue()
    {
        return pointsValue;
    }
}
