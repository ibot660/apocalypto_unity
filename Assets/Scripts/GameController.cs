﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private Player_Controller player = null;

    [SerializeField] public GZombie_Spawner factory1 = null;
    [SerializeField] public GZombie_Spawner factory2 = null;
    [SerializeField] public GZombie_Spawner factory3 = null;

    [SerializeField] public GZombie_Spawner factory4 = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (factory1.numberOfLives() <= 0)
        {
            Destroy(factory1);
        }
        if (factory2.numberOfLives() <= 0)
        {
            Destroy(factory2);
        }
        if (factory3.numberOfLives() <= 0)
        {
            Destroy(factory3);
        }
        if (factory4.numberOfLives() <= 0)
        {
            Destroy(factory4);
        }
        if (player.numberOfLives() <= 0)
        {
            // Game Over
            SceneManager.LoadScene("GameOver-Lose");
        }
        else
        {
            if (factory1.numberOfLives() <= 0 && factory2.numberOfLives() <= 0 && factory3.numberOfLives() <= 0)
                SceneManager.LoadScene("SampleScene");
            else
            {
             if (factory1.numberOfLives() <= 0 && factory2.numberOfLives() <= 0 && factory4.numberOfLives() <= 0)
             SceneManager.LoadScene("Win");
            }
        }
        
    }

    public void updatePlayerScore(int points)
    {
        if (player)
        {
            player.updateScore(points);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        

    }

    
}
