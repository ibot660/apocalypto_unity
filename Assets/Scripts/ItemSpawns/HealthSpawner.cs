﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSpawner : MonoBehaviour
{
    [SerializeField] private GameObject Health = null;
    [SerializeField] private float SpawnTime = 8.0f;

    [SerializeField] private GameController control = null;

    private float countdown = 5.0f;
    System.Random rndGen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void SpawnHealth()
    {
        Vector2 Spoint = transform.position;
        GameObject HealthItem = Instantiate(Health, Spoint, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        void Update()
        {
            countdown -= Time.deltaTime;
            

            rndGen = new System.Random();


            if (countdown <= 0.0f)
            {
                SpawnHealth();
                countdown = SpawnTime;
            }
        }
    }
}
