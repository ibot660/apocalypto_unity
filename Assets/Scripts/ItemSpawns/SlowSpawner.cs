﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowSpawner : MonoBehaviour
{
    [SerializeField] private GameObject Slow = null;
    [SerializeField] private float SpawnTime = 5.0f;

    [SerializeField] private GameController control = null;

    private float countdown = 5.0f;
    System.Random rndGen;
    // Start is called before the first frame update
    void Start()
    {

    }

    void SpawnSlow()
    {
        Vector2 Spoint = transform.position;
        GameObject SlowItem = Instantiate(Slow, Spoint, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        void Update()
        {
            countdown -= Time.deltaTime;
            

            rndGen = new System.Random();


            if (countdown <= 0.0f)
            {
                SpawnSlow();
                countdown = SpawnTime;
            }
        }
    }
}
