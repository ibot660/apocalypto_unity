﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_target : MonoBehaviour
{
    [SerializeField] private Transform target;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        Vector3 tempz = new Vector3(0, 0, -15f);
        transform.position += tempz;

    }

    // Update is called once per frame
    void Update()
    {
        var target_pos = target.position;
        var camera_pos = transform.position;
        camera_pos.x = target_pos.x;
        camera_pos.y = target_pos.y;
        transform.position = camera_pos;




    }
}
