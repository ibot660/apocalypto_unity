﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track_Mouse : MonoBehaviour
{
    [SerializeField] private Transform target;


    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void trackMouse()
    {
        //Get the Screen positions of the object
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

        //Get the Screen position of the mouse
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        //Get the angle between the points
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);

        //Ta Daaa
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));

        float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
        {
            return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
        }
    }

    void followPlayer()
    {
        var target_pos = target.position;
        var sprite_pos = transform.position;
        sprite_pos.x = target_pos.x;
        sprite_pos.y = target_pos.y;
        transform.position = sprite_pos;
    }


    // Update is called once per frame
    void Update()
    {
        trackMouse();
        followPlayer();
    }
}
