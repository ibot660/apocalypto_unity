﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprite_Fire : MonoBehaviour
{
    [SerializeField] private GameObject bullet1 = null;
    [SerializeField] private GameObject Laser1 = null;
    [SerializeField] private float fireDelayTimePistol = 1.0f;
    [SerializeField] private float fireDelayTimeRifle = 1.0f;
    [SerializeField] private float fireDelayTimeLaser = 1.0f;
    private AudioSource audiosrc;
    private float fireTime = 0.0f;
    private float bulletForce = 15f;
    private float equipedWeapon = 0;
    public Sprite pistolSprite;
    public Sprite rifleSprite;
    public Sprite laserSprite;


    // Start is called before the first frame update
    void Start()
    {
        audiosrc = GetComponent<AudioSource>();
    }

    void switchWeapon()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            switch (equipedWeapon)
            {
                case 0:
                    equipedWeapon = equipedWeapon + 1;
                    this.GetComponent<SpriteRenderer>().sprite = rifleSprite;
                    break;
                case 1:
                    equipedWeapon = equipedWeapon + 1;
                    this.GetComponent<SpriteRenderer>().sprite = laserSprite;
                    break;
                case 2:
                    equipedWeapon = equipedWeapon - 2;
                    this.GetComponent<SpriteRenderer>().sprite = pistolSprite;
                    break;
            }

        }
    }

    void playerFirePistol()
    {
        if (equipedWeapon <= 1)
        {
            // Decrease fire countdown
            fireTime -= Time.deltaTime;

            // Only fire a bullet if we press the fire button (referred to input "Fire1" in the Unity Input Manager) AND our fire countdown has reached or passed 0 (remember this controls the fire rate)
            if (Input.GetButton("Fire1") && fireTime <= 0.0f)
            {
                // Use the bulletPrefab game object reference to create a new bullet at the player's position and orientation
                GameObject bullet = Instantiate(bullet1, transform.position, transform.rotation);
                Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                rb.AddForce(-transform.right * bulletForce, ForceMode2D.Impulse);

                // The bullet game object will contain the Bullet_Behave component - get this and call the OwningPlayer property to set the owner of the bullet to the current player
                bullet.GetComponent<Bullet_Behave>().OwningPlayer = this;

                // Get the audio source attached to the player and play the fire sound.

                audiosrc.Play();


                // Reset the fire countdown
                fireTime = fireDelayTimePistol;
            }
        }
    }

    void playerFireRifle()
    {
        if (equipedWeapon == 1)
        {
            // Decrease fire countdown
            fireTime -= Time.deltaTime;

            // Only fire a bullet if we press the fire button (referred to input "Fire1" in the Unity Input Manager) AND our fire countdown has reached or passed 0 (remember this controls the fire rate)
            if (Input.GetButton("Fire1") && fireTime <= 0.0f)
            {
                // Use the bulletPrefab game object reference to create a new bullet at the player's position and orientation
                GameObject bullet = Instantiate(bullet1, transform.position, transform.rotation);
                Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                rb.AddForce(-transform.right * bulletForce, ForceMode2D.Impulse);

                // The bullet game object will contain the Bullet_Behave component - get this and call the OwningPlayer property to set the owner of the bullet to the current player
                bullet.GetComponent<Bullet_Behave>().OwningPlayer = this;

                // Get the audio source attached to the player and play the fire sound.

                audiosrc.Play();


                // Reset the fire countdown
                fireTime = fireDelayTimeRifle;
            }
        }
    }

    void playerFirelaser()
    {
        if (equipedWeapon >= 2)
        {
            fireTime -= Time.deltaTime;
            // Only fire a bullet if we press the fire button (referred to input "Fire1" in the Unity Input Manager) AND our fire countdown has reached or passed 0 (remember this controls the fire rate)
            if (Input.GetButton("Fire1") && fireTime <= 0.0f)
            {
                // Use the bulletPrefab game object reference to create a new bullet at the player's position and orientation
                GameObject laser = Instantiate(Laser1, transform.position, transform.rotation);


                Debug.Log("Hello");
                // The bullet game object will contain the Bullet_Behave component - get this and call the OwningPlayer property to set the owner of the bullet to the current player
                laser.GetComponent<Laser_Behave>().OwningPlayer = this;

                // Get the audio source attached to the player and play the fire sound.

                audiosrc.Play();
                fireTime = fireDelayTimeLaser;

            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (!PauseMenu.GameIsPaused)
        {
            playerFirePistol();
            switchWeapon();
            playerFireRifle();
            playerFirelaser();
        }
    }
}
