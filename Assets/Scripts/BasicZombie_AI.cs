﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class BasicZombie_AI : MonoBehaviour
{
    [SerializeField] private float speed = 3.0f;
    [SerializeField] private float stoppingDistance = 6.0f;
    [SerializeField] private Transform target;
    [SerializeField] private Animator animator = null;
    [SerializeField] private int pointsValue = 1;
    //[SerializeField] private Transform player;
    private Rigidbody2D rb;


    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (dead = false)
        //{
        Vector3 direction = target.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rb.rotation = angle;


        //if (Vector2.Distance(transform.position, target.position) > stoppingDistance)
        //{
        //transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        //}
        //}
    }

    void OnTriggerEnter2D(Collider2D collision)
    {

        // Different objects might collide with the invader - by assigning tags we can work out which object collided...
        // Here we're only interested in player bullets - everything else is okay :)
        // So, if the invader is part of a fleet and a player bullet hit...
        if (collision.tag == "Player_bullet")
        {

            //dead = true;
            Destroy(GetComponent<BoxCollider2D>());
            if (animator)
            {
                animator.SetBool("IsAlive", false);
            }

            AudioSource audio = GetComponent<AudioSource>();
            if (audio)
            {
                audio.Play();
            }

            // Remove the bullet - we don't want super bullets that pass through invaders without stopping - would be nice though :)
            Destroy(collision.gameObject);

        }
        if (collision.tag == "Player_laser")
        {

            //dead = true;

            if (animator)
            {
                animator.SetBool("IsAlive", false);
            }


            AudioSource audio = GetComponent<AudioSource>();
            if (audio)
            {
                audio.Play();
            }

        }
    }

    public void destroyGZombie()
    {
        Destroy(gameObject);
    }

    public int getPointsValue()
    {
        return pointsValue;
    }
}
