﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser_Behave : MonoBehaviour
{
    private Sprite_Fire owner;
    public float TimeToLive = 5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public Sprite_Fire OwningPlayer
    {
        get { return owner; }
        set { owner = value; }
    }


    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, TimeToLive);
    }
}
